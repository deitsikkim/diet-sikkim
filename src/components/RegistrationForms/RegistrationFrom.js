import React from 'react';
import {Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {Link} from 'react-router-dom';
import $ from "jquery";
import toastr from "toastr";
import ENV from "../utils/Constant";
import DataSource from "../utils/DataSource";
class RegistrationFrom extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            toggle: 'random',
            districts:DataSource.Districts,
            subDivisions:[],
            optedColleges: [],
            district:'',
            subdivision:'',
            optedCollege: '',
            showDesignationModal:false,
            buttonText: 'SUBMIT APPLICATION',
            candidateName: '',
            fatherName: '',
            motherName:'',
            communicationAddress:'',
            candidateMobileNumber:'',
            category: '',
            aadharNumber:'',
            emailId:'',
            gender:'',
            dob: '',
            pwd: '',
            passingYear:'',
            board: '',
            subject1:'',
            subject2:'',
            subject3:'',
            subject4:'',
            subject5:'',
            subject6:'',
            coiNumber: '',
            coiHolderName: '',
            coiDateOfIssue: '',
            coiOwner: '',
            regexNumberCheck: /^[0-9]+$/,
            regexAlphabetCheck: /^[a-zA-Z ]*$/
        }
        this.handleInputChange =this.handleInputChange.bind(this);
        this.handleSubmit =this.handleSubmit.bind(this);
        this.success = this.success.bind(this);
        this.failure = this.failure.bind(this);
        this.handleDropdown = this.handleDropdown.bind(this);
        this.onHandleNumberChange = this.onHandleNumberChange.bind(this);
        this.onHandleAlphabetChange = this.onHandleAlphabetChange.bind(this);
    }
    success() {
        this.props.history.push("/success");
    }
    failure() {
        this.props.history.push("/failure");
    }

    handleSubmit(e) {
        e.preventDefault();
        let dataObj = {
            toggle: this.state.toggle,
            district: this.state.district,
            subdivision: this.state.subdivision,
            optedCollege: this.state.optedCollege,
            candidateName: this.state.candidateName,
            fatherName: this.state.fatherName,
            motherName: this.state.motherName,
            communicationAddress: this.state.communicationAddress,
            candidateMobileNumber: this.state.candidateMobileNumber,
            category: this.state.category,
            aadharNumber: this.state.aadharNumber,
            emailId: this.state.emailId,
            gender: this.state.gender,
            dob: this.state.dob,
            pwd: this.state.pwd,
            passingYear: this.state.passingYear,
            board: this.state.board,
            subject1: this.state.subject1,
            subject2: this.state.subject2,
            subject3: this.state.subject3,
            subject4: this.state.subject4,
            subject5: this.state.subject5,
            subject6: this.state.subject6,
            coiNumber: this.state.coiNumber,
            coiHolderName: this.state.coiHolderName,
            coiDateOfIssue: this.state.coiDateOfIssue,
            coiOwner: this.state.coiOwner
        }
        console.log(this.state);
        let self = this;
        $.ajax({
                url: ENV.baseUrl+'/registration.php',
                method:'POST',
                data: dataObj,
                success: function(response){
                    console.log(response);
                    let data = JSON.parse(response);
                    if (data["message"] === "duplicate") {
                        toastr.error("This aadhar has been registered.Try with another!");
                        self.failure();
                    } if(data["message"] === "ok") {
                        toastr.success("Saved Successfully");
                        self.success();
                    }
                }.bind(this),
                error: function(error){
                    toastr.error("Refresh and Try again");
                    self.failure();
                }.bind(this)
            }

        )
    }
    handleDropdown(name, value) {
        if(name === 'district') {
            let subDistrictData = DataSource.SubDistrictMap[value];
            let collegeData = DataSource.OptedCollegeMap[value];
            this.setState({
                subDivisions:subDistrictData,
                optedColleges:collegeData
            });
        }

    }
    onHandleAlphabetChange(e) {
        const target = e.target;
        const name = target.name;
        const alphabet = target.value;
        if (alphabet === '' || this.state.regexAlphabetCheck.test(alphabet)) {
            this.setState({ [name]: alphabet })
        }
        console.log(name,alphabet);
    }

    onHandleNumberChange(e) {
        const target = e.target;
        const name = target.name;
        const numeric = target.value;
        if (numeric === '' || this.state.regexNumberCheck.test(numeric)) {
            this.setState({ [name]: numeric })
        }
        console.log(name,numeric);
    };


    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        },()=>this.handleDropdown(name,value));
        console.log(name,value);
    }

    render()
    {
        let districts = this.state.districts.map((district) =>
            <option key={district.districtId} value={district.districtId}>{district.districtName}</option>
        );

        let subdivisions = this.state.subDivisions.map((subdivision,i)=>
            <option key={i} value={subdivision}>{subdivision}</option>
        );

        let colleges = this.state.optedColleges.map((college,i)=>
            <option key={i} value={college}>{college}</option>
        );
        return (

            <div className="mainTabContainer">
                <div className="col-xs-12">
                    <Breadcrumb>
                        <BreadcrumbItem>
                            <Link to="/home" style={{color: '#44a7c7', cursor: 'pointer'}}>Home</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem active>
                            Registration From
                        </BreadcrumbItem>
                    </Breadcrumb>
                    <div className="container">
                        <form>

                        </form>
                    </div>
                    <div className="container">
                        <form onSubmit={this.handleSubmit}>
                            <div className="card" id="box">
                                <div className="card-body">
                                    <div className="row">
                                        <div className="col-sm">
                                            <div className="form-group">
                                                <label htmlFor="toggle">Do you have COI? choose your option</label>
                                                <select
                                                    name="toggle"
                                                    className="custom-select"
                                                    onChange={this.handleInputChange}
                                                    value={this.state.toggle}
                                                    required={true}
                                                >
                                                    <option value="random" selected>Select</option>
                                                    <option value="yes">YES</option>
                                                    <option value="no">NO</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <br/>
                            {
                                this.state.toggle === 'yes' ?
                                    <div className="side1">
                                        <div className="card" id="box">
                                            <div className="card-header" style={{backgroundColor: '#9A1750'}}>
                                                <h5 className="text-white">COI DETAILS</h5>
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lcoinumber">COI number<span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter COI number"
                                                                   className="form-control alphabets"
                                                                   name="coiNumber"
                                                                   value={this.state.coiNumber}
                                                                   id="lcoinumber"
                                                                   maxLength="20"
                                                                   onChange={this.handleInputChange}
                                                                   required={true} />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lcoiholder">COI holder name<span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter name of COI holder"
                                                                   className="form-control"
                                                                   name="coiHolderName"
                                                                   value={this.state.coiHolderName}
                                                                   id="lcoiholder"
                                                                   maxLength="30"
                                                                   onChange={this.onHandleAlphabetChange}
                                                                   required={true} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="ldateofissue">COI date of issue<span style={{color:'red'}}>*</span></label>
                                                            <input type="date"
                                                                   className="form-control"
                                                                   name="coiDateOfIssue"
                                                                   value={this.state.coiDateOfIssue}
                                                                   id="ldateofissue"
                                                                   onChange={this.handleInputChange}
                                                                   required={true} />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lcoiowner">Choose COI owner<span style={{color:'red'}}>*</span></label>
                                                            <select
                                                                name="coiOwner"
                                                                className="custom-select"
                                                                value={this.state.coiOwner}
                                                                onChange={this.handleInputChange}
                                                                required={true}
                                                                id="lcoiowner"
                                                            >
                                                                <option value="">Select</option>
                                                                <option value="SELF">SELF</option>
                                                                <option value="FATHER">FATHER</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                            </div>

                                        </div>
                                        <br/>
                                        <div className="card" id="box">
                                            <div className="card-header" style={{backgroundColor: '#9A1750'}}>
                                                <h5 className="text-white">PERSONAL DETAILS</h5>
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="district">Choose your district<span style={{color:'red'}}>*</span></label>
                                                            <select
                                                                name="district"
                                                                className="custom-select"
                                                                value={this.state.district}
                                                                onChange={this.handleInputChange}
                                                                required={true}
                                                                id="district"
                                                            >
                                                                <option value="" disabled={true}>Select</option>
                                                                {districts}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="subdivision">Choose your sub-division<span style={{color:'red'}}>*</span></label>
                                                            <select
                                                                name="subdivision"
                                                                className="custom-select"
                                                                value={this.state.subdivision}
                                                                onChange={this.handleInputChange}
                                                                required={true}
                                                                id="subdivision"
                                                            >
                                                                <option value="" disabled={true}>Select</option>
                                                                {subdivisions}
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="optedCollege">Opted College<span style={{color:'red'}}>*</span></label>
                                                            <select
                                                                name="optedCollege"
                                                                className="custom-select"
                                                                value={this.state.optedCollege}
                                                                onChange={this.handleInputChange}
                                                                required={true}
                                                                id="optedCollege"
                                                            >
                                                                <option value="" disabled={true}>Select</option>
                                                                {colleges}
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lsname">Name of Candidate <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter name of candidate"
                                                                   className="form-control alphabets"
                                                                   name="candidateName"
                                                                   value={this.state.candidateName}
                                                                   id="lsname"
                                                                   onChange={this.onHandleAlphabetChange}
                                                                   required={true} />
                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lfname">Father's name <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter father's name"
                                                                   className="form-control"
                                                                   name="fatherName"
                                                                   value={this.state.fatherName}
                                                                   id="lfname"
                                                                   onChange={this.onHandleAlphabetChange}  required={true} />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lmname">Mother's name <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter mother's name"
                                                                   className="form-control"
                                                                   name="motherName"
                                                                   value={this.state.motherName}
                                                                   id="lmname"
                                                                   onChange={this.onHandleAlphabetChange}  required={true} />
                                                        </div>
                                                    </div>


                                                </div>

                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="laddress">Permanent address<span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter permanent address"
                                                                   className="form-control"
                                                                   name="communicationAddress"
                                                                   value={this.state.communicationAddress}
                                                                   id="laddress"
                                                                   onChange={this.handleInputChange}  required={true} />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="aadharnumber">Aadhar Number<span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter 12-digit Aadhar Number"
                                                                   className="form-control"
                                                                   name="aadharNumber"
                                                                   id="aadharnumber"
                                                                   maxLength="12"
                                                                   pattern="[0-9]*"
                                                                   value={this.state.aadharNumber}
                                                                   onChange={this.onHandleNumberChange}  required={true}
                                                            />
                                                        </div>
                                                    </div>

                                                </div>

                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lmn">Mobile number(Whatsapp number) <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter 10-digit mobile number"
                                                                   className="form-control"
                                                                   name="candidateMobileNumber"
                                                                   id="lmn"
                                                                   value={this.state.candidateMobileNumber}
                                                                   maxLength="10"
                                                                   onChange={this.onHandleNumberChange}  required={true} />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lemail">Email Address<span style={{color:'red'}}>*</span></label>
                                                            <input type="email"
                                                                   placeholder="Enter email address"
                                                                   id="lemail"
                                                                   className="form-control"
                                                                   name="emailId"
                                                                   value={this.state.emailId}
                                                                   onChange={this.handleInputChange}
                                                                   required={true}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lcategory">Choose your category<span style={{color:'red'}}>*</span></label>
                                                            <select
                                                                name="category"
                                                                className="custom-select"
                                                                value={this.state.category}
                                                                onChange={this.handleInputChange}
                                                                required={true}
                                                                id="lcategory"
                                                            >
                                                                <option value="" selected={true}>Select</option>
                                                                <option value="STATE OBC">STATE OBC</option>
                                                                <option value="CENTRAL OBC">CENTRAL OBC</option>
                                                                <option value="ST">ST</option>
                                                                <option value="SC">SC</option>
                                                                <option value="BL">BL</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="gender">Gender <span style={{color:'red'}}>*</span></label>
                                                            <select
                                                                name="gender"
                                                                className="custom-select"
                                                                value={this.state.gender}
                                                                onChange={this.handleInputChange}
                                                                required={true}
                                                                id="gender"
                                                            >
                                                                <option value="" selected={true}>Select</option>
                                                                <option value="MALE">MALE</option>
                                                                <option value="FEMALE">FEMALE</option>
                                                                <option value="OTHERS">OTHERS</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lpwd">Person with disabilities (pwd)<span style={{color:'red'}}>*</span></label>
                                                            <select
                                                                name="pwd"
                                                                className="custom-select"
                                                                value={this.state.pwd}
                                                                onChange={this.handleInputChange}
                                                                required={true}
                                                                id="lpwd"
                                                            >
                                                                <option value="" selected={true}>Select</option>
                                                                <option value="YES">YES</option>
                                                                <option value="NO">NO</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="ldob">Date of birth<span style={{color:'red'}}>*</span></label>
                                                            <input
                                                                type="date"
                                                                name="dob"
                                                                className="form-control"
                                                                id="ldob"
                                                                value={this.state.dob}
                                                                onChange={this.handleInputChange}
                                                                required={true}
                                                            />
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                        <div className="card" id="box">
                                            <div className="card-header" style={{backgroundColor: '#9A1750'}}>
                                                <h5 className="text-white">EDUCATION DETAILS</h5>
                                            </div>
                                            <div className="card-body">
                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="board">Board <span style={{color:'red'}}>*</span></label>
                                                            <select
                                                                name="board"
                                                                className="custom-select"
                                                                value={this.state.board}
                                                                onChange={this.handleInputChange}
                                                                required={true}
                                                                id="board"
                                                            >
                                                                <option value="" selected={true}>Select</option>
                                                                <option value="CBSE">CBSE</option>
                                                                <option value="ICSE">ICSE</option>
                                                                <option value="OTHERS">OTHERS</option>
                                                            </select>
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lyp">Year of Passing <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter the year of passing"
                                                                   className="form-control"
                                                                   name="passingYear"
                                                                   id="lyp"
                                                                   maxLength="4"
                                                                   value={this.state.passingYear}
                                                                   onChange={this.onHandleNumberChange}
                                                                   required={true} />
                                                        </div>
                                                    </div>
                                                </div>
                                                <br/>
                                                <h6 className="text-danger">Note:Enter the marks in descending order (The subject in which marks obtained is highest should be entered at first)</h6>
                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lsubject1">Enter subject and marks(Eg:History/78)  <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter subject name 1/marks"
                                                                   className="form-control"
                                                                   name="subject1"
                                                                   maxLength="20"
                                                                   id="lsubject1"
                                                                   value={this.state.subject1}
                                                                   onChange={this.handleInputChange}  required={true} />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lsubject2">Enter subject and marks(Eg:History/78)  <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter subject name 2/marks"
                                                                   className="form-control"
                                                                   name="subject2"
                                                                   maxLength="20"
                                                                   id="lsubject2"
                                                                   value={this.state.subject2}
                                                                   onChange={this.handleInputChange}  required={true} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lsubject3">Enter subject and marks(Eg:History/78)  <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter subject name 3/marks"
                                                                   className="form-control"
                                                                   name="subject3"
                                                                   maxLength="20"
                                                                   id="lsubject3"
                                                                   value={this.state.subject3}
                                                                   onChange={this.handleInputChange}  required={true} />
                                                        </div>
                                                    </div>

                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lsubject4">Enter subject and marks(Eg:History/78)  <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter subject name 4/marks"
                                                                   className="form-control"
                                                                   name="subject4"
                                                                   maxLength="20"
                                                                   id="lsubject4"
                                                                   value={this.state.subject4}
                                                                   onChange={this.handleInputChange}  required={true} />
                                                        </div>
                                                    </div>
                                                </div>

                                                <div className="row">
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lsubject5">Enter subject and marks(Eg:History/78)  <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter subject name 5/marks"
                                                                   className="form-control"
                                                                   name="subject5"
                                                                   maxLength="20"
                                                                   id="lsubject5"
                                                                   value={this.state.subject5}
                                                                   onChange={this.handleInputChange}  required={true} />
                                                        </div>
                                                    </div>
                                                    <div className="col-sm">
                                                        <div className="form-group">
                                                            <label htmlFor="lsubject6">Enter subject and marks(Eg:History/78)  <span style={{color:'red'}}>*</span></label>
                                                            <input type="text"
                                                                   placeholder="Enter subject name 6/marks"
                                                                   className="form-control"
                                                                   name="subject6"
                                                                   maxLength="20"
                                                                   id="lsubject6"
                                                                   value={this.state.subject6}
                                                                   onChange={this.handleInputChange}  required={true} />
                                                        </div>
                                                    </div>

                                                </div>
                                                <div className="col-sm d-flex justify-content-end">
                                                    <div className="form-group">
                                                        <button
                                                            className="btn btn-success btn-large"
                                                            type="submit"
                                                            disabled={false}
                                                        >
                                                            <i className="fa fa-check-circle"/> {this.state.buttonText}
                                                        </button>
                                                    </div>
                                                </div>
                                            </div>
                                        </div>
                                        <br/>
                                    </div>
                                :   [   this.state.toggle === 'no' ?
                                            <div className="side1">
                                                <div className="card" id="box" style={{backgroundColor: '#9A1750'}}>
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col">
                                                                <center>
                                                                    <h5 className="special text-white-50">SORRY, YOU ARE NOT ELIGIBLE. APPLICATIONS ARE INVITED FROM LOCAL CANDIDATES ONLY</h5>
                                                                    <div className="regular-pic">
                                                                        <img className="text-white" src={'assets/assets/image/sorry.png'} alt="Picture showing we're sorry" id="regularPic"/>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                        :
                                            <div className="side1">
                                                <div className="card" id="box" style={{backgroundColor: '#9A1750'}}>
                                                    <div className="card-body">
                                                        <div className="row">
                                                            <div className="col">
                                                                <center>
                                                                    <h5 className="special text-white-50">WELCOME</h5>
                                                                    <p className="text-white">Please select an option above to continue.</p>
                                                                    <p className="text-white">For more information regarding colleges download the prospectus by clicking on the button below</p>
                                                                    <div className="row">
                                                                        <div className="col-sm">
                                                                            <a className="btn btn-success" href={'assets/assets/downloads/DIET_sikkim_prospectus_2020.pdf'} download><i className="fa fa-download"/>&nbsp; Download prospectus</a>
                                                                        </div>
                                                                        <div className="col-sm">
                                                                            <a className="btn btn-success" href={'assets/assets/downloads/DIET_sikkim_admission_notice_2020.pdf'} download><i className="fa fa-download"/>&nbsp; Download admission notice</a>
                                                                        </div>
                                                                    </div>
                                                                </center>
                                                            </div>
                                                        </div>
                                                    </div>
                                                </div>
                                            </div>
                                    ]
                            }
                        </form>
                        <br/>
                        <br/>
                    </div>
                </div>
            </div>



        )
    }

}
export default RegistrationFrom
