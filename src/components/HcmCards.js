import React,{Component} from "react";
export default class HcmCards extends React.Component {
    render() {
        return (
            <div>

                <div className="card rounded" id="box">
                    <div className="card-body">
                        <div className="row">
                            <div className="col-sm">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-sm">
                                            <div className="regular-pic">
                                                <img className="border" src={'assets/assets/image/DIET1.jpeg'} alt="Picture of DIET college, East Sikkim"/>
                                            </div>
                                            <br/>
                                            <h6 className="text-center text-muted">District Institute of Education & Training (DIET) East Sikkim, Gangtok</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-sm">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-sm">
                                            <div className="regular-pic">
                                                <img className="border" src={'assets/assets/image/DIET2.jpeg'} alt="Picture of DIET college, west sikkim"/>
                                            </div>
                                            <br/>
                                            <h6 className="text-muted text-center">District Institute of Education & Training (DIET) West Sikkim, Gyalshing</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div className="col-sm">
                                <div className="container">
                                    <div className="row">
                                        <div className="col-sm">
                                            <div className="regular-pic">
                                                <img className="border" src={'assets/assets/image/DIET3.jpeg'} alt="Picture of DIET college, south sikkim"/>
                                            </div>
                                            <br/>
                                            <h6 className="text-center text-muted">District Institute of Education & Training (DIET) South Sikkim, Namchi</h6>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        );
    }

}
