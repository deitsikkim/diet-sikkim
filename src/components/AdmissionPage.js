import React from 'react';
import Modal from 'react-bootstrap4-modal';
import Instruction from "./Instruction";
import {Breadcrumb, BreadcrumbItem} from "reactstrap";
import {Link} from "react-router-dom";

export default class AdmissionPage extends React.Component  {
    constructor(props) {
        super(props);
        this.state = {
            showApplyModal:false,
            showRegisteredModal: false
        }
        this.hideApplyModal =this.hideApplyModal.bind(this);
        this.showApplyModal =this.showApplyModal.bind(this);
    }
    showApplyModal(){
        this.setState({showApplyModal:true});
    }
    hideApplyModal(){
        this.setState({showApplyModal:false});
    }
    render() {
        return (
            <div>
                <div className="mainTabContainer">
                    <Breadcrumb>
                        <BreadcrumbItem>
                            <Link to="/home" style={{color: '#44a7c7', cursor: 'pointer'}}>Home</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem active>
                            Guidelines
                        </BreadcrumbItem>
                    </Breadcrumb>
                    <div className="container text-center">
                        <h3>Application Form for Admission in Class XI</h3>
                        <h5>(For Government Senior Secondary Schools of Sikkim only)</h5>
                        <br/>
                        <Instruction/>
                        <br/>
                        <div className="row">
                            <div className="col-sm">
                                <button href="#" className="btn btn-large btn-info" id="tab2" disabled={true}>
                                    <i className="fa fa-plus"/> Open school registration
                                </button>
                            </div>
                            <div className="col-sm">
                                <button className="btn btn-large btn-info" onClick={() => this.showApplyModal()} id="tab2" disabled={true}>
                                    <i className="fa fa-plus"/> <span>Regular school registration</span>
                                </button>
                            </div>
                        </div>
                        <br/>
                        <br/>
                        <br/>
                        <p><strong className="text-center text-danger">Online Registration process for students who have passed class X from open school has been closed. You may contact the school directly for your admission. Thank you.</strong></p>
                        <p><strong className="text-center text-danger">Online Registration process for students who have passed class X from regular school has been closed.</strong></p>
                        <br/>

                        <div>
                            <Modal visible={this.state.showApplyModal} id="designationModal" dialogClassName="modal-lg">
                                <form onSubmit="">
                                    <div className="modal-header">
                                        <h4 className="modal-title">Select District</h4>
                                        <button type="button" className="close" data-dismiss="modal" onClick={this.hideApplyModal}>&times;</button>
                                    </div>
                                    <div className="modal-body">
                                        <div>
                                            <div className="card">
                                                <div className="card-body">
                                                    <div className="row text-center">
                                                        <div className="col-sm">
                                                            <a className="btn btn-info btn-large" href="/west-registration-form" id="tab2">
                                                                <i className="fa fa-arrow-circle-o-left" style={{fontSize: 25}}></i> West</a>
                                                        </div>
                                                        <div className="col-sm">
                                                            <a className="btn btn-info btn-large" href="/north-registration-form" id="tab2">
                                                                <i className="fa fa-arrow-circle-o-up" style={{fontSize: 25}}></i>  North</a>
                                                        </div>
                                                        <div className="col-sm">
                                                            <a className="btn btn-info btn-large" href="/south-registration-form" id="tab2">
                                                                <i className="fa fa-arrow-circle-o-down" style={{fontSize: 25}}></i> South</a>
                                                        </div>
                                                        <div className="col-sm">
                                                            <a className="btn btn-info btn-large" href="/east-registration-form" id="tab2">
                                                                <i className="fa fa-arrow-circle-o-right" style={{fontSize: 25}}></i> East</a>
                                                        </div>
                                                    </div>
                                                    <br/>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </form>
                            </Modal>
                        </div>
                    </div>
                </div>
                <br/>
                <br/>
                <br/>
            </div>
        );
    }
}
