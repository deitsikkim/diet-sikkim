import React from 'react';
import {Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {NavLink,Link} from 'react-router-dom';
import $ from "jquery";
import toastr from 'toastr';
import ENV from "../utils/Constant";
import Modal from "react-bootstrap4-modal";
import EditRegistration from "../EditModals/EditRegistration";
class RegisteredLists extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            filter: '',
            searchButtonText: 'SEARCH',
            editButtonText: 'EDIT DETAILS',
            details: {
                candidateName: '',
                fatherName: '',
                motherName:'',
                communicationAddress:'',
                candidateMobileNumber:'',
                aadharNumber:'',
                emailId:'',
                gender:'',
                passingYear:'',
                board: '',
                subject1:'',
                subject2:'',
                subject3:'',
                subject4:'',
                subject5:'',
                subject6:'',
                district:'',
                subDivision:'',
                optedCollege:'',
                category:''
            },
            empty: '',
            regexNumberCheck: /^[0-9]+$/,
            showModal: false,

        }
        this.onHandleNumberChange =this.onHandleNumberChange.bind(this);
        this.handleSubmit =this.handleSubmit.bind(this);
        this.hideEditModal = this.hideEditModal.bind(this);
        this.showEditModal = this.showEditModal.bind(this);
    }
    hideEditModal(){
        this.setState({showModal: false});
    }
    showEditModal(){
        this.setState({showModal: true});
    }

    updateDetails = (updatedDetails) => {
        this.setState({
            details: {
                candidateName: updatedDetails.studentName,
                fatherName: updatedDetails.fatherName,
                motherName: updatedDetails.motherName,
                communicationAddress: updatedDetails.address,
                candidateMobileNumber: updatedDetails.mobileNumber,
                aadharNumber: updatedDetails.aadharNumber,
                emailId: updatedDetails.email,
                gender: updatedDetails.gender,
                passingYear: updatedDetails.passingYear,
                board: updatedDetails.board,
                subject1: updatedDetails.sub1,
                subject2: updatedDetails.sub2,
                subject3: updatedDetails.sub3,
                subject4: updatedDetails.sub4,
                subject5: updatedDetails.sub5,
                subject6: updatedDetails.sub6,
                district: updatedDetails.district_name,
                subDivision: updatedDetails.subdistrict,
                optedCollege: updatedDetails.optedCollege,
                category: updatedDetails.category,
            }
        })
    }

    handleSubmit(e) {
        e.preventDefault();
        let dataObj = {
            filter: this.state.filter
        }
        console.log(this.state);
        let self = this;
        $.ajax({
                url: ENV.baseUrl+'/fetchData.php',
                method:'POST',
                data: dataObj,
                success: function(response){
                    let data = JSON.parse(response);
                    console.log(data);
                    if(data["message"] === "no") {
                        this.setState({empty: 'Dear student! seems like you have not been registered. Please register as soon as possible.'});
                    } else {
                        this.setState({
                            details: {
                                candidateName: data.studentName,
                                fatherName: data.fatherName,
                                motherName: data.motherName,
                                communicationAddress: data.address,
                                candidateMobileNumber: data.mobileNumber,
                                aadharNumber: data.aadharNumber,
                                emailId: data.email,
                                gender: data.gender,
                                passingYear: data.passingYear,
                                board: data.board,
                                subject1: data.sub1,
                                subject2: data.sub2,
                                subject3: data.sub3,
                                subject4: data.sub4,
                                subject5: data.sub5,
                                subject6: data.sub6,
                                district: data.district_name,
                                subDivision: data.subdistrict,
                                optedCollege: data.optedCollege,
                                category: data.category
                            }
                        });
                    }

                }.bind(this),
                error: function(error){
                    console.log(error);
                }.bind(this)
            }

        )
    }

    onHandleNumberChange(e) {
        const target = e.target;
        const name = target.name;
        const numeric = target.value;
        if (numeric === '' || this.state.regexNumberCheck.test(numeric)) {
            this.setState({ [name]: numeric })
        }
        console.log(name,numeric);
    };
    render()
    {
        return (

            <div className="mainTabContainer">
                <div className="col-xs-12">
                    <Breadcrumb>
                        <BreadcrumbItem>
                            <Link to="/home" style={{color: '#44a7c7', cursor: 'pointer'}}>Home</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem active>
                            Registered List
                        </BreadcrumbItem>
                    </Breadcrumb>
                    <div className="container">
                        <div className="card">
                            <div className="card-body">
                                <p className="text-danger">MESSAGE: Enter Aadhar Number below to check if you're registered.</p>
                                <form onSubmit={this.handleSubmit}>
                                    <div className="row">
                                        <div className="col-sm">
                                            <div className="form-group">
                                                <label htmlFor="filter">Registered Aadhar Number</label>
                                                <input
                                                    type="text"
                                                    className="form-control"
                                                    name="filter"
                                                    id="filter"
                                                    maxLength="12"
                                                    value={this.state.filter}
                                                    placeholder="Enter Aadhar Number"
                                                    onChange={this.onHandleNumberChange}
                                                    required={true}
                                                />
                                            </div>
                                        </div>
                                    </div>
                                    <div className="row">
                                        <div className="col-sm d-flex justify-content-start">
                                            <div className="form-group">
                                                <button
                                                    className="btn btn-success btn-large"
                                                    type="submit"
                                                    name="Submit"
                                                    id="createDesignation"
                                                >
                                                    <i className="fa fa-check-circle"/> {this.state.searchButtonText}
                                                </button>
                                            </div>
                                        </div>
                                    </div>
                                </form>
                                <br/>
                                { this.state.empty === '' ?
                                    <div className="displayContainer">
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>District: <strong>{this.state.details.district}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Sub-Division: <strong>{this.state.details.subDivision}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>Opted College: <strong>{this.state.details.optedCollege}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Name of Candidate: <strong>{this.state.details.candidateName}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>Father's Name: <strong>{this.state.details.fatherName}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Mother's Name: <strong>{this.state.details.motherName}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>Permanent Address: <strong>{this.state.details.communicationAddress}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Aadhar Number: <strong>{this.state.details.aadharNumber}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>Candidate's Mobile Number: <strong>{this.state.details.candidateMobileNumber}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Email: <strong>{this.state.details.emailId}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>Category: <strong>{this.state.details.category}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Gender: <strong>{this.state.details.gender}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>Subject 1: <strong>{this.state.details.subject1}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Subject 2: <strong>{this.state.details.subject2}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>Subject 3: <strong>{this.state.details.subject3}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Subject 4: <strong>{this.state.details.subject4}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>Subject 5: <strong>{this.state.details.subject5}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Subject 6: <strong>{this.state.details.subject6}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <div className="col-sm">
                                                <p>Board type: <strong>{this.state.details.board}</strong></p>
                                            </div>
                                            <div className="col-sm">
                                                <p>Year of Passing: <strong>{this.state.details.passingYear}</strong></p>
                                            </div>
                                        </div>
                                        <div className="row">
                                            <button
                                                className="btn btn-success"
                                                onClick={() => this.showEditModal()} >
                                                <i className="fa fa-edit" /> {this.state.editButtonText}
                                            </button>
                                        </div>
                                    </div>
                                    :
                                    <div className="displayContainer">
                                        <div className="col-sm">
                                            <p className="text-danger">{this.state.empty}</p>
                                        </div>
                                    </div>
                                }
                            </div>
                        </div>
                        <EditRegistration
                            data={this.state.details}
                            updateDetails={this.updateDetails}
                            showHide={this.state.showModal}
                            hide={this.hideEditModal}
                        />
                        <br/>
                        <br/>
                    </div>
                </div>
                <br/>
            </div>



        )
    }

}
export default RegisteredLists
