import React from "react";
import {Breadcrumb, BreadcrumbItem} from "reactstrap";
import {Link} from "react-router-dom";
export default class Success extends React.Component {
    render() {
        return (
            <div>
                <Breadcrumb>
                    <BreadcrumbItem>
                        <Link to="/home" style={{color: '#44a7c7', cursor: 'pointer'}}>Home</Link>
                    </BreadcrumbItem>
                </Breadcrumb>
                <div className="container">
                    <div className="card">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-sm">
                                    <div className="container-pic">
                                        <img src={'assets/assets/image/success.jpg'} id="imageCards" alt="Image depicting a thank you message." />
                                    </div>
                                </div>
                                <div className="col-sm align-self-center">
                                    <div className="card" id="box" style={{backgroundColor: '#9A1750'}}>
                                        <div className="card-body">
                                            <h5 className="text-white">Hello There! Your application has been submitted successfully. Please check your registered email, a confirmation mail has been sent.</h5>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                </div>
            </div>
        );
    }

}
