import React from "react";
export default class Instruction extends React.Component{

    render() {
        return (
            <div>
                <div className="card">
                    <div className="card-header bg-info">
                        NOTICE
                    </div>
                    <div className="card-body">
                        <ol className="text-center">
                            <li className="text-left">
                                Filling up the application form does not confirm your admission in opted Streams.
                            </li>
                            <li className="text-left">
                                Those students who have passed class X from Senior Secondary School must
                                apply in their respective schools only.
                            </li>
                            <li className="text-left">
                                Those students who have passed class X from Senior Secondary School
                                can apply to other nearby Senior Secondary Schools if their preferred Streams are not
                                available at home schools.
                            </li>
                            <li className="text-left">
                                Students must enter the correct information in the application form.
                            </li>
                            <li className="text-left">
                                Admission in Science & Commerce Streams shall be as per the criteria
                                laid down by the respective schools. Preference for admission shall
                                be given to the feeder schools & the other schools in the surrounding
                                depending upon the availability of seats.
                            </li>
                            <li className="text-left">
                                Compartment candidates of Senior Secondary Schools can apply for
                                provisional admission in their own schools.
                                The students of Secondary Schools can apply at nearby school;
                                however, the school will have the final decision.
                            </li>
                            <li className="text-left">
                                The decision of the admission committee of the respective school will be final.
                            </li>
                        </ol>
                    </div>
                </div>
            </div>
        );
    }

}
