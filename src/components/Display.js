import React from 'react';
import {Breadcrumb, BreadcrumbItem} from 'reactstrap';
import {NavLink,Link} from 'react-router-dom';
import $ from "jquery";
import toastr from 'toastr';
import ENV from "./utils/Constant";
class Display extends React.Component {
    constructor(props) {
        super(props);
        this.state = {
            school: '',

        }
        this.handleInputChange =this.handleInputChange.bind(this);
        this.handleSubmit =this.handleSubmit.bind(this);
    }

    handleSubmit(e) {
        e.preventDefault();
        let dataObj = {
            school:this.state.school
        }
        let self = this;
        $.ajax({
                url: ENV.baseUrl+'/registrationPortal.php',
                method:'POST',
                data: dataObj,
                success: function(response){
                    console.log(response);
                    let data = JSON.parse(response);
                    if(data["message"] === "ok") {
                        toastr.success("Saved Successfully");
                    } else {

                    }
                }.bind(this),
                error: function(error){
                    console.log(error);
                    toastr.error("Sorry! could not be saved");
                }.bind(this)
            }

        )
    }

    handleInputChange(event) {
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
        console.log(name,value);
    }
    render()
    {
        return (

            <div className="mainTabContainer">
                <div className="col-xs-12">
                    <Breadcrumb>
                        <BreadcrumbItem>
                            <Link to="/home" style={{color: '#44a7c7', cursor: 'pointer'}}>Dashboard</Link>
                        </BreadcrumbItem>
                        <BreadcrumbItem active>
                            Admin Panel
                        </BreadcrumbItem>
                    </Breadcrumb>

                    <div className="container">
                        <div className="card">
                            <div className="card-body">
                                <form onSubmit={this.handleSubmit}>
                                    <div className="row">
                                        <div className="col-sm">
                                            <div className="form-group">
                                                <label htmlFor="select">Select The school</label>
                                                <select
                                                    name="schools"
                                                    className="form-control custom-select"
                                                    value={this.state.school}
                                                    onChange={this.handleInputChange}
                                                    required={true}
                                                    id="select"
                                                >
                                                    <option disabled={true} selected={true}>Select the school From Below</option>
                                                    <option value="37225">GOVT. SENIOR SECONDARY SCHOOL, ASSAM LINGZEY</option>
                                                    <option value="37217">BIRASPATI PARSAI GOVT. SENIOR SECONDARY SCHOOL</option>
                                                    <option value="37352">GOVT. SENIOR SECONDARY SCHOOL, BOJOGHARI</option>
                                                    <option value="37204">TIKALAL NIROULA GOVT. SENIOR SECONDARY SCHOOL, CENTRAL PENDAM</option>
                                                    <option value="37211">GOVT. SENIOR SECONDARY SCHOOL, CHUJACHEN</option>
                                                    <option value="37223">DEORALI GIRLS GOVT. SENIOR SECONDARY SCHOOL</option>
                                                    <option value="37208">GOVT. SENIOR SECONDARY SCHOOL, DIKCHU</option>
                                                    <option value="37219">GOVT. SENIOR SECONDARY SCHOOL, DIKLING</option>
                                                    <option value="37224">GOVT. SENIOR SECONDARY SCHOOL, ENCHEY</option>
                                                    <option value="37202">SONAMATI MEMORIAL GOVT. SENIOR SECONDARY SCHOOL, KHAMDONG</option>
                                                    <option value="37248">GOVT. SENIOR SECONDARY SCHOOL, LINGDOK</option>
                                                    <option value="37280">GOVT. SENIOR SECONDARY SCHOOL, MACHONG</option>
                                                    <option value="37308">GOVT. SENIOR SECONDARY SCHOOL, MAKHA</option>
                                                    <option value="37209">GOVT. SENIOR SECONDARY SCHOOL, MAMRING</option>
                                                    <option value="37220">MODERN GOVT. SENIOR SECONDARY SCHOOL</option>
                                                    <option value="37210">GOVT. SENIOR SECONDARY SCHOOL, RANGPO</option>
                                                    <option value="37206">GOVT. SENIOR SECONDARY SCHOOL, RANKA</option>
                                                    <option value="37213">GOVT. SENIOR SECONDARY SCHOOL, RHENOCK</option>
                                                    <option value="37216">GOVT. SENIOR SECONDARY SCHOOL, RUMTEK</option>
                                                    <option value="37207">GOVT. SENIOR SECONDARY SCHOOL, SAMDONG</option>
                                                    <option value="37214">GOVT. SENIOR SECONDARY SCHOOL, SANG</option>
                                                    <option value="37203">GOVT. SENIOR SECONDARY SCHOOL, SINGTAM</option>
                                                    <option value="37271">GOVT. SENIOR SECONDARY SCHOOL, TADONG</option>
                                                    <option value="37205">GOVT. SENIOR SECONDARY SCHOOL, WEST POINT</option>
                                                    <option value="37290">GOVT. SENIOR SECONDARY SCHOOL, LUING</option>
                                                    <option value="37348">GOVT. SENIOR SECONDARY SCHOOL, ROLEP</option>
                                                    <option value="37373">GOVT. SENIOR SECONDARY SCHOOL, PACHEY</option>
                                                    <option value="37325">GOVT. SENIOR SECONDARY SCHOOL, PABYUIK</option>
                                                    <option value="37289">GOVT. SENIOR SECONDARY SCHOOL, NAMCHEYBONG</option>
                                                    <option value="37326">GOVT. SENIOR SECONDARY SCHOOL, SICHEY</option>
                                                    <option value="37359">GOVT. SENIOR SECONDARY xSCHOOL, RORATHANG</option>
                                                </select>
                                            </div>
                                        </div>
                                    </div>
                                    <div className="col-sm d-flex justify-content-end">
                                        <div className="form-group">
                                            <button
                                                className="btn btn-success btn-large"
                                                type="submit" name="Submit"
                                                id="createDesignation"
                                                disabled={this.state.isDisabled}>
                                                <i className="fa fa-check-circle"></i>  {this.state.buttonText}
                                            </button>
                                        </div>
                                    </div>

                                </form>
                            </div>
                        </div>

                    </div>
                </div>
                <br/>
            </div>



        )
    }

}
export default Display
