import React, {Component} from "react";
import {Link} from "react-router-dom";
import HcmCards from "./HcmCards";

export default class Home extends Component
{
    constructor(props) {
        super(props);
        this.state = {
            showApplyModal:false
        }
    }
    render() {
        return(
            <div>
                <div className="container text-center">
                    <HcmCards/>
                    <br/>
                    <div className="card" id="box">
                        <div className="card-header text-white" style={{backgroundColor: '#9A1750'}}>
                            <h5 className="special text-white-50">INSTRUCTIONS</h5>
                        </div>
                        <div className="card-body">
                            <div className="container">
                                <div className="row">
                                    <p className="text-justify text-muted">
                                        We are aware about the COVID-19 pandemic which has
                                        spread all over the world. Together with the nation,
                                        our State is also gripped in the clutches of this pandemic
                                        which has led us to follow lockdown in our State as per
                                        the guidelines of disaster management authority.
                                    </p>
                                    <p className="text-justify text-muted">
                                        Since the class XII results of school have been already
                                        declared by all boards, ensuring continuity of teaching
                                        learning process without any obstacle is our priority.
                                        Keeping all the facts in view, the Education Department has
                                        initiated the process of online admission in all the DIET
                                        college of State. Candidates seeking DIET college are therefore
                                        requested to read the prospectus carefully and apply
                                        for admission by 10th September 2020.
                                    </p>
                                </div>

                            </div>

                        </div>
                    </div>
                    <br/>
                    <div className="card" id="box">
                        <div className="card-header text-white" style={{backgroundColor: '#9A1750'}}>
                            <h5 className="special text-white-50">TO-DO</h5>
                        </div>
                        <div className="card-body">
                            <div className="row text-center">
                                <div className="col-sm">
                                    <a
                                        className="btn btn-success btn-large"
                                        href="/registration-list"
                                        id="tab3">
                                        <i className="fas fa-binoculars" style={{fontSize: '20px'}}/>&nbsp; View your Registration</a>
                                </div>
                                <div className="col-sm">
                                    <a
                                        className="btn btn-success btn-large"
                                        href="/registration-form"
                                        id="tab3">
                                        <i className="fas fa-clipboard-check" style={{fontSize: '20px'}}/>&nbsp;  Fill up your application</a>
                                </div>
                                <div className="col-sm">
                                    <a
                                        className="btn btn-success"
                                        href={'assets/assets/downloads/DIET_sikkim_prospectus_2020.pdf'}
                                        id="tab3"
                                        download>
                                        <i className="fa fa-download" style={{fontSize: '20px'}}/>&nbsp; Download prospectus</a>
                                </div>

                                <div className="col-sm">
                                    <a
                                        className="btn btn-success"
                                        href={'assets/assets/downloads/DIET_sikkim_admission_notice_2020.pdf'}
                                        id="tab2"
                                        download>
                                        <i className="fa fa-download" style={{fontSize: '20px'}}/>&nbsp; Download admission notice</a>
                                </div>
                            </div>
                            <br/>
                            <p className="text-center text-muted">For more details or technical issues please contact us at: +91 9564994956 / +91 8327467816 / +91 8768468598</p>
                        </div>
                    </div>
                    <br/>
                    <br/>
                </div>
            </div>
        )
    }
}
