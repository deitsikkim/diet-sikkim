import React,{Component} from "react";
import Carousel from 'react-bootstrap/Carousel'
export default class Image extends React.Component{

    render() {
        return (
            <div className="container" >
                <Carousel>
                    {/*First SLide*/}
                    <Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:50,paddingBottom:90}}
                            className="d-block w-100"
                            src={'assets/assets/image/5.jpeg'}
                            alt="Third slide"
                        />
                        {/*<Carousel.Caption>
                        </Carousel.Caption>*/}
                    </Carousel.Item>
                    {/*Second SLide*/}
                    {/*<Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:50,paddingBottom:90}}
                            className="d-block w-100"
                            src={'assets/assets/image/4.jpeg'}
                            alt="Third slide"
                        />
                    </Carousel.Item>*/}
                    {/*third SLide*/}
                    <Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:5,paddingBottom:5}}
                            className="d-block w-100"
                            src={'assets/assets/image/west.jpeg'}
                            alt="First slide"
                        />
                        {/*<Carousel.Caption>
                        </Carousel.Caption>*/}
                    </Carousel.Item>
                    {/*fourth SLide*/}
                    <Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:50,paddingBottom:90}}
                            className="d-block w-100"
                            src={'assets/assets/image/east.jpeg'}
                            alt="First slide"
                        />
                        {/*<Carousel.Caption>
                        </Carousel.Caption>*/}
                    </Carousel.Item>
                    {/*fifth SLide*/}
                    <Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:10,paddingBottom:10}}
                            className="d-block w-100"
                            src={'assets/assets/image/south.jpeg'}
                            alt="First slide"
                        />
                        {/*<Carousel.Caption>
                        </Carousel.Caption>*/}
                    </Carousel.Item>
                    {/*sixth SLide*/}
                    <Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:50,paddingBottom:90}}
                            className="d-block w-100"
                            src={'assets/assets/image/north.jpeg'}
                            alt="First slide"
                        />
                        {/*<Carousel.Caption>
                        </Carousel.Caption>*/}
                    </Carousel.Item>
                    {/*seventh SLide*/}
                    <Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:10,paddingBottom:10}}
                            className="d-block w-100"
                            src={'assets/assets/image/1.jpeg'}
                            alt="First slide"
                        />
                        {/*<Carousel.Caption>
                        </Carousel.Caption>*/}
                    </Carousel.Item>
                    {/*eight SLide*/}
                    <Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:10,paddingBottom:10}}
                            className="d-block w-100"
                            src={'assets/assets/image/2.jpeg'}
                            alt="Third slide"
                        />
                        {/*<Carousel.Caption>
                        </Carousel.Caption>*/}
                    </Carousel.Item>
                    {/*ninth SLide*/}
                    <Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:50,paddingBottom:90}}
                            className="d-block w-100"
                            src={'assets/assets/image/3.jpeg'}
                            alt="Third slide"
                        />
                        {/*<Carousel.Caption>
                        </Carousel.Caption>*/}
                    </Carousel.Item>
                    {/*tenth SLide*/}
                    <Carousel.Item>
                        <img
                            style={{paddingLeft:10,paddingRight:10,paddingTop:50,paddingBottom:90}}
                            className="d-block w-100"
                            src={'assets/assets/image/6.jpeg'}
                            alt="Third slide"
                        />
                        {/*<Carousel.Caption>
                        </Carousel.Caption>*/}
                    </Carousel.Item>
                </Carousel>
            </div>
        );
    }
}
