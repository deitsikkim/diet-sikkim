const DataSource = {
    Districts: [
        {'districtId': 'NS', 'districtName': 'North Sikkim'},
        {'districtId': 'WS', 'districtName': 'West Sikkim'},
        {'districtId': 'SS', 'districtName': 'South Sikkim'},
        {'districtId': 'ES', 'districtName': 'East Sikkim'}
    ],

    SubDistrictMap: {
        'NS': ['Chungthang', 'Dzongu', 'Kabi', 'Mangan'],
        'WS': ['Gyalshing', 'Soreng', 'Yuksom', 'Dentam'],
        'SS': ['Namchi', 'Jorethang', 'Ravangla', 'Yangyang'],
        'ES': ['Gangtok', 'Pakyong', 'Rongli', 'Rangpo']
    },

    OptedCollegeMap: {
        'NS': ['DIET NAMCHI/GYALSHING'],
        'WS': ['DIET GYALSHING'],
        'SS': ['DIET NAMCHI'],
        'ES': ['DIET GANGTOK']
    }
}
export default DataSource;



