import React from 'react';
import Modal from 'react-bootstrap4-modal';
import $ from "jquery";
import toastr from "toastr";
import ENV from '../utils/Constant';

export default class EditRegistration extends React.Component {
    constructor(props){
        super(props);
        this.state = {
            updateButtonText: 'UPDATE DETAILS',
            data: '',
            key: '',
            key_candidateName: '',
            key_fatherName: '',
            key_motherName: '',
            key_PermanentAddress: '',
            key_candidateMobile: '',
            key_category: '',
            key_gender: '',
            key_subject1: '',
            key_subject2: '',
            key_subject3: '',
            key_subject4: '',
            key_subject5: '',
            key_subject6: '',
            key_email: '',
            key_passingYear: '',
            key_board: '',
            key_college:'',
            key_district:'',
            key_subDivision:'',
            regexNumberCheck: /^[0-9]+$/,
            regexAlphabetCheck: /^[a-zA-Z ]*$/
        }
        this.handleInput = this.handleInput.bind(this);
        this.handleSubmit = this.handleSubmit.bind(this);
        this.onHandleAlphabetChange = this.onHandleAlphabetChange.bind(this);
        this.onHandleNumberChange = this.onHandleNumberChange.bind(this);
    }

    componentWillReceiveProps(props) {
        if (props.data !== undefined) {
            this.setState({
                key: props.data.aadharNumber,
                key_candidateName: props.data.candidateName,
                key_fatherName: props.data.fatherName,
                key_motherName: props.data.motherName,
                key_PermanentAddress: props.data.communicationAddress,
                key_candidateMobile: props.data.candidateMobileNumber,
                key_category: props.data.category,
                key_gender: props.data.gender,
                key_subject1: props.data.subject1,
                key_subject2: props.data.subject2,
                key_subject3: props.data.subject3,
                key_subject4: props.data.subject4,
                key_subject5: props.data.subject5,
                key_subject6: props.data.subject6,
                key_email: props.data.emailId,
                key_passingYear: props.data.passingYear,
                key_board: props.data.board,
                key_college: props.data.optedCollege,
                key_district: props.data.district,
                key_subDivision: props.data.subDivision
            });
        } else {
            this.setState({
                key: '',
                key_name: ''
            })
        }
    }
    onHandleAlphabetChange(e) {
        const target = e.target;
        const name = target.name;
        const alphabet = target.value;
        if (alphabet === '' || this.state.regexAlphabetCheck.test(alphabet)) {
            this.setState({ [name]: alphabet })
        }
        console.log(name,alphabet);
    }

    onHandleNumberChange(e) {
        const target = e.target;
        const name = target.name;
        const numeric = target.value;
        if (numeric === '' || this.state.regexNumberCheck.test(numeric)) {
            this.setState({ [name]: numeric })
        }
        console.log(name,numeric);
    };

    handleInput(event){
        const target = event.target;
        const value = target.type === 'checkbox' ? target.checked : target.value;
        const name = target.name;
        this.setState({
            [name]: value
        });
        console.log(name,value);
    }


    handleSubmit(e){
        e.preventDefault();
        console.log(this.state);
        console.log(this.props.data);
        let dataObj = {
            key: this.state.key,
            key_candidateName: this.state.key_candidateName,
            key_fatherName: this.state.key_fatherName,
            key_motherName: this.state.key_motherName,
            key_PermanentAddress: this.state.key_PermanentAddress,
            key_candidateMobile: this.state.key_candidateMobile,
            key_category: this.state.key_category,
            key_gender: this.state.key_gender,
            key_subject1: this.state.key_subject1,
            key_subject2: this.state.key_subject2,
            key_subject3: this.state.key_subject3,
            key_subject4: this.state.key_subject4,
            key_subject5: this.state.key_subject5,
            key_subject6: this.state.key_subject6,
            key_email: this.state.key_email,
            key_passingYear: this.state.key_passingYear,
            key_board: this.state.key_board
        }
        $.ajax({
            url: ENV.baseUrl+'/updateDetails.php',
            method:'POST',
            data: dataObj,
            success: function(response){
                console.log(response);
                let data = JSON.parse(response);
                console.log(data);
                toastr.success("Your details have been updated!");
                this.props.updateDetails(data);
                this.props.hide();
            }.bind(this),
            error: function(error){
                toastr.error("Please try again!");
            }.bind(this)
        });

    }
    render() {
        const modalStyle = {
            width: "100%",
            maxWidth: "400px",
            padding: "20px 10px 20px 10px",
            margin: "0px auto 0px auto"
        }
        return(
            <Modal visible={this.props.showHide} dialogClassName="modal-md">
                <form onSubmit={this.handleSubmit}>
                    <div className="modal-header">
                        <h5 className="modal-title special"> EDIT INFORMATION </h5>
                        <button
                            type="button"
                            className="btn"
                            data-dismiss="modal"
                            onClick={()=>this.props.hide()}>
                            <i className="fas fa-window-close" style={{fontSize: '40px', color:'#dc3545'}}/>
                        </button>
                    </div>

                    <div className="modal-body" style={modalStyle} >
                        {/*<div className="row">
                            <p className="text-danger">NOTE: For editing aadhar number, opted school and stream please contact +918327467816 / +918768468598</p>
                        </div>*/}
                        <div className="form-group">
                            <div>
                                <label htmlFor="key" >Aadhar Number</label>
                                <input
                                    type="text"
                                    id="key"
                                    className="form-control"
                                    name="key"
                                    value={this.state.key}
                                    readOnly={true}
                                />
                            </div>
                            <div>
                                <label htmlFor="lcollege" >Opted College</label>
                                <input
                                    type="text"
                                    id="lcollege"
                                    className="form-control"
                                    name="key_college"
                                    value={this.state.key_college}
                                    readOnly={true}
                                />
                            </div>

                            <div>
                                <label htmlFor="ldistrict" >District</label>
                                <input
                                    type="text"
                                    id="ldistrict"
                                    className="form-control"
                                    name="key_district"
                                    value={this.state.key_district}
                                    readOnly={true}
                                />
                            </div>
                            <div>
                                <label htmlFor="lsubdivision" >Sub Division</label>
                                <input
                                    type="text"
                                    id="lsubdivision"
                                    className="form-control"
                                    name="key_subDivision"
                                    value={this.state.key_subDivision}
                                    readOnly={true}
                                />
                            </div>


                            <div className="form-group">
                                <label htmlFor="key_name" >Name of Candidate:</label>
                                <input
                                    type="text"
                                    id="key_name"
                                    className="form-control"
                                    name="key_candidateName"
                                    maxLength="30"
                                    value={this.state.key_candidateName}
                                    onChange={this.onHandleAlphabetChange}

                                />
                            </div>
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_fatherName" >Father's Name</label>
                            <input
                                type="text"
                                id="key_fatherName"
                                className="form-control"
                                name="key_fatherName"
                                maxLength="30"
                                value={this.state.key_fatherName}
                                onChange={this.onHandleAlphabetChange}

                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_motherName" >Mother's Name</label>
                            <input
                                type="text"
                                id="key_motherName"
                                className="form-control"
                                name="key_motherName"
                                maxLength="40"
                                value={this.state.key_motherName}
                                onChange={this.onHandleAlphabetChange}

                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_PermanentAddress" >Permanent Address</label>
                            <input
                                type="text"
                                id="key_PermanentAddress"
                                className="form-control"
                                name="key_PermanentAddress"
                                maxLength="50"
                                value={this.state.key_PermanentAddress}
                                onChange={this.handleInput}/>

                        </div>
                        <div className="form-group">
                            <label htmlFor="key_email">Email</label>
                            <input
                                type="email"
                                id="key_email"
                                className="form-control"
                                name="key_email"
                                value={this.state.key_email}
                                onChange={this.handleInput}

                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_candidateMobile">Candidate's Mobile Number</label>
                            <input
                                type="text"
                                id="key_candidateMobile"
                                className="form-control"
                                name="key_candidateMobile"
                                maxLength="10"
                                value={this.state.key_candidateMobile}
                                onChange={this.onHandleNumberChange}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_category">Category</label>
                            <input
                                type="text"
                                id="key_category"
                                className="form-control"
                                name="key_category"
                                maxLength="10"
                                value={this.state.key_category}
                                onChange={this.onHandleAlphabetChange}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_gender">Gender</label>
                            <input
                                type="text"
                                id="key_gender"
                                className="form-control"
                                name="key_gender"
                                maxLength="10"
                                value={this.state.key_gender}
                                onChange={this.onHandleAlphabetChange}
                            />
                        </div>

                        <div className="form-group">
                            <label htmlFor="key_subject1" >Subject 1</label>
                            <input
                                type="text"
                                id="key_subject1"
                                className="form-control"
                                name="key_subject1"
                                maxLength="20"
                                value={this.state.key_subject1}
                                onChange={this.handleInput}

                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_subject2" >Subject 2</label>
                            <input
                                type="text"
                                id="key_subject2"
                                className="form-control"
                                name="key_subject2"
                                maxLength="20"
                                value={this.state.key_subject2}
                                onChange={this.handleInput}

                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_subject3" >Subject 3</label>
                            <input
                                type="text"
                                id="key_subject3"
                                className="form-control"
                                name="key_subject3"
                                maxLength="20"
                                value={this.state.key_subject3}
                                onChange={this.handleInput}

                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_subject4" >Subject 4</label>
                            <input
                                type="text"
                                id="key_subject4"
                                className="form-control"
                                name="key_subject4"
                                maxLength="20"
                                value={this.state.key_subject4}
                                onChange={this.handleInput}
                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_subject5" >Subject 5</label>
                            <input
                                type="text"
                                id="key_subject5"
                                className="form-control"
                                name="key_subject5"
                                maxLength="20"
                                value={this.state.key_subject5}
                                onChange={this.handleInput}

                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_subject6">Subject 6</label>
                            <input
                                type="text"
                                id="key_subject6"
                                className="form-control"
                                name="key_subject6"
                                maxLength="20"
                                value={this.state.key_subject6}
                                onChange={this.handleInput}

                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_board">Board</label>
                            <input
                                type="text"
                                id="key_board"
                                className="form-control"
                                name="key_board"
                                maxLength="20"
                                value={this.state.key_board}
                                onChange={this.onHandleAlphabetChange}

                            />
                        </div>
                        <div className="form-group">
                            <label htmlFor="key_passingYear" >Year of Passing</label>
                            <input
                                type="text"
                                id="key_passingYear"
                                className="form-control"
                                name="key_passingYear"
                                maxLength="4"
                                value={this.state.key_passingYear}
                                onChange={this.onHandleNumberChange}
                            />
                        </div>
                        <div className="col-sm d-flex justify-content-end">
                            <div className="form-group">
                                <button
                                    type="submit"
                                    className="btn btn-success"
                                    name="submit"
                                >
                                    <i className="fa fa-save"/> {this.state.updateButtonText}

                                </button>
                            </div>
                        </div>

                    </div>
                </form>
            </Modal>

        );
    }


}
