import React,{Component} from "react";
export default class MobileVerification extends React.Component{
    render(){
        return(
            <div className="mainTabContainer">
                <div className="pull-right">
                    <div>
                        <button className="btn btn-outline-info" onClick={()=>this.props.history.goBack()}>
                            <i className="fa fa-backward"></i> Back</button>
                    </div>
                </div>
                    Mobile Number Verification
                    <hr/>
                  <div className="container">
                           <section id="cover" className="min-vh-100">
                               <div id="cover-caption">
                                   <div className="container">
                                       <div className="row text-white">
                                           <div
                                               className="col-xl-5 col-lg-6 col-md-8 col-sm-10 mx-auto text-center form p-4">
                                               <h1 className="display-4 py-2 text-truncate">Center my form.</h1>
                                               <div className="px-2">
                                                   <form action="" className="justify-content-center">
                                                       <div className="form-group">
                                                           <label className="sr-only">Mobile Number</label>
                                                           <input type="number" className="form-control"
                                                                  placeholder="Mobile Number"/>
                                                       </div>
                                                       <button type="submit" className="btn btn-primary btn-lg">Submit
                                                       </button>
                                                   </form>
                                               </div>
                                           </div>
                                       </div>
                                   </div>
                               </div>
                           </section>
                       </div>
            </div>
        );
    }

}
