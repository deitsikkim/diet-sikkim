import React, {Component} from "react";


export default class Footer extends Component
{
    render() {
        return(
            <div>
                <div className="footer">
                    <p>Developed by <a href="https://bolds.in/" id="links" target="_blank">Bolds Innovation</a> </p>
                </div>
            </div>

        )
    }
}
