import React from "react";
import {Breadcrumb, BreadcrumbItem} from "reactstrap";
import {Link} from "react-router-dom";
export default class Failure extends React.Component {
    render() {
        return (
            <div>
                <Breadcrumb>
                    <BreadcrumbItem>
                        <Link to="/home" style={{color: '#44a7c7', cursor: 'pointer'}}>Home</Link>
                    </BreadcrumbItem>
                </Breadcrumb>
                <div className="container">
                    <div className="card">
                        <div className="card-body">
                            <div className="row">
                                <div className="col-sm">
                                    <div className="container-pic">
                                        <img src={'assets/assets/image/failure.jpg'} id="imageCards" alt="jason" />
                                    </div>
                                </div>

                                <div className="col-sm align-self-center">
                                    <div className="row justify-content-center">
                                        <div className="card" id="box" style={{backgroundColor: '#9A1750'}}>
                                            <div className="card-body">
                                                <h5 className="text-white special">LOOKS LIKE A FAILURE </h5>
                                            </div>
                                        </div>
                                    </div>
                                    <br/>
                                    <div className="row justify-content-center">
                                        <div className="card" id="box" style={{backgroundColor: '#9A1750'}}>
                                            <div className="card-body">
                                                <h5 className="text-white">Sorry, your application has not been submitted.Please go back to the form and try submitting it again</h5>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <br/><br/>
                </div>
            </div>
        );
    }

}
