import React, {Component} from "react";
import { withRouter } from 'react-router';
class Header extends Component
{
    render() {
        const logo = require('../images/sikkimLogo.png');
        return(
            <div className="mainTabContainer">
                <header>
                    <div className="bg-warning">
                        <marquee className="text-danger">The online application for D.EI.Ed course in DIET Sikkim is open. The last date of application is 10<sup>th</sup> September, 2020.</marquee>
                    </div>
                    <nav className="navbar navbar-expand-sm navbar-dark" style={{backgroundColor: '#9A1750', paddingTop: '30px', paddingBottom:'30px'}}>
                        <div className="container-pic">
                            <a className="navbar-brand" href="#"><img src={'assets/assets/image/logo.png'} id="avatar" alt="jason" /></a>
                        </div>
                        <h5 className="text-white-50 text-center special">DISTRICT INSTITUTE OF EDUCATION AND TRAINING</h5>
                    </nav>
                </header>
            </div>
        )
    }
}
export default withRouter(Header);
