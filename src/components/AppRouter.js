import React, {Component} from "react";
import {BrowserRouter as Router,Route,Switch} from "react-router-dom";
import Home from "./Home";
import Header from "./Header";
import Footer from "./Footer";
import RegistrationFrom from "./RegistrationForms/RegistrationFrom";
import RegisteredLists from "./RegisteredLists/RegisteredLists";
import Failure from "./Failure";
import Success from "./Success";
class AppRouter extends Component{
    render() {
        return(
            <div>
                <Router>
                    <Header/>
                    <br/>
                    <Switch>
                        <Route path="/" exact component={Home}/>
                        <Route path="/home" exact component={Home}/>
                        <Route path="/registration-form" exact component={RegistrationFrom}/>
                        <Route path="/registration-list" exact component={RegisteredLists}/>
                        <Route path="/failure" exact component={Failure}/>
                        <Route path="/success" exact component={Success}/>
                    </Switch>
                    <br/>
                    <Footer/>
                </Router>
            </div>
        );
    }
}



/*
function ShowInvalidCredentials(props)
{
    if(props.hasLogInFailed)
    {
        return <div>Invalid Credentials</div>
    }
  else
    {
        return null;
    }
}
function ShowLoginSuccess(props) {

    if(props.loginSuccess)
    {
        return  <div>Login Sucessful</div>
    }
    else
    {
        return null;
    }

}*/
export default AppRouter;
